import React, { useState, useEffect } from 'react'

import * as resources from '../../resources/index'

import { Box, CircularProgress, TextField } from '@material-ui/core'

import { Grid, Card, CardActionArea, CardMedia, Typography, CardContent, CardActions, Button,makeStyles  } from '@material-ui/core'

const useStyles = makeStyles({
  root: {
    width: 345,
    margin: '30px',
    backgroundColor: '#f6f6f6'
  },
  media: {
    height: 140,
  },
});


const Characters = () => {
  const [response, setResponse] = useState([])
  const [offset, setOffset] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const classes = useStyles();

  const isBottom = (el) => {
    return el.getBoundingClientRect().bottom <= window.innerHeight;
  }
  const dig = (obj, tag) => {
    try{
      return obj[tag]
    } catch {
      return []
    }
  }
  const trackScrolling = () => {
    const wrappedElement = document.getElementById('root');
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      setIsLoading(true)
      loadData()
    }
  };

  document.addEventListener('scroll', trackScrolling);

  const loadData = async (search = null) => {
    if(isLoading){
      const res = await resources.marvel.get(offset, search)
      let aux = response
      if(offset === aux.length) {
        aux.push(...dig(res.data, 'results'))
        setResponse(aux)
        setOffset(aux.length)
      }
      setIsLoading(false)
    }
  }

  if(isLoading === true){
    loadData()
  }

  return (
    <>
      <Box>
        <Grid container justify justifyItems="space-around">
        {response.map((obj => (
            <Grid item xl={4} lg={4} md={6} sm={12} xs={12} key={obj.name}>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image={`${obj.thumbnail.path}.${obj.thumbnail.extension}`}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {obj.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {obj.description}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
              <Button size="small" href={obj.urls[0].url}>
                Ver mais
              </Button>
            </CardActions>
            </Card>
          </Grid>
        )))}
        {isLoading && 
          <Grid 
            container
            direction="row"
            justify="center"
            alignItems="center"
          >
              <CircularProgress color='secondary'/>
          </Grid>
        }
        </Grid>
      </Box>
    )
    </>
  )
}

export default Characters

