import Axios from 'axios'
import CryptoJS from 'crypto-js/'

const timestamp = new Date().getTime();

const PRIVATE_KEY = '2ea8a7191482b9d2eb796fa68a524e9c7298c213';
const PUBLIC_KEY = '69cc3fea04d203d46d549b0761b6d9a4';
const hash = CryptoJS.MD5(timestamp.toString() + PRIVATE_KEY + PUBLIC_KEY).toString()
const urlBase = `https://gateway.marvel.com/v1/public/characters`

const get = async (offset, search = null) => {
  try {
    const response = await Axios.get(urlBase, {
      params: {
        offset: offset,
        apikey: PUBLIC_KEY,
        nameStartsWith: search,
        hash: hash,
        ts: timestamp.toString()
      }
    })
    return response.data
  } catch(err) {
    return err
  }
}

export default { get }

